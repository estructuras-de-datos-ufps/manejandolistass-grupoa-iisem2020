/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class TestLista_Borrar {
    public static void main(String[] args) {
        ListaS<Integer> enteros=new ListaS();
        enteros.insertarFinal(20);
        enteros.insertarFinal(200);
        enteros.insertarFinal(2000);
        enteros.insertarFinal(20000);
        System.out.println(enteros.toString());
        System.out.println("Borrando de la lista el nodo de posición 2");
        System.out.println(enteros.eliminar(2));
        System.out.println("Lista sin la pos 2: "+enteros.toString());
    }
}
