/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Modelo;

/**
 *
 * @author madar
 */
public class Persona implements Comparable{
    
    private int cedula;
    private String nombre;
    private int d,m,a; //Fecha de nacimiento

    public Persona(int cedula, String nombre, int d, int m, int a) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.d = d;
        this.m = m;
        this.a = a;
    }


    
    
    public Persona() {
    }

    public Persona(int cedula, String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + this.cedula;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.cedula != other.cedula) {
            return false;
        }
        return true;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    @Override
     /**
      * Método realizado por el estudiante Gederson Guzman
      */
    public int compareTo(Object o){
            Persona obj = (Persona)o;
            
            int thisEdad = (this.a * 10000) + (this.m * 100) + this.d;
            int otherEdad = (obj.a * 10000) + (obj.m * 100) + obj.d;
            System.out.println("this edad : " + thisEdad + "  otra :  " + otherEdad);
            if(thisEdad > otherEdad)
                return -1;
            else
                if(thisEdad < otherEdad)
                    return 1;
            return 0;
            // return thisEdad-otherEdad
    }

    
    
    
    
    
}
